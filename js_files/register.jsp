<%@ page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta charset="UTF-8">
  <title>daniSyrel_project</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel="stylesheet" href="../css_files/login_and_signup.css">
</head>

<script language="javascript">
function check_username(username)
{
    //check if the username contains only letters and spaces
    for(i = 0; i < username.length; i++)
    {
        if((username.charAt(i) < 'A' || username.charAt(i) > 'Z') && (username.charAt(i) < 'a' || username.charAt(i) > 'z') && username.charAt(i) != ' ')
        {
            return false;
        }
    }
    return true;
}

function check_password(password)
{
    var small_letters_flag = false;
    var capital_letters_flag = false;
    var numbers_flag = false;
    //check passwords' length 
    if(password.length < 8)
    {
        return false;
    }
    for(i = 0; i < password.length; i++)
    {
        if(password.charAt(i) >= 'a' && password.charAt(i) <= 'z')
        {
            small_letters_flag = true;
        }
        else if(password.charAt(i) >= 'A' && password.charAt(i) <= 'Z')
        {
            capital_letters_flag = true;
        }
        else if(password.charAt(i) >= '0' && password.charAt(i) <= '9')
        {
            numbers_flag = true;
        }
        else
        {
            return false;
        }
    }
    if(numbers_flag && capital_letters_flag && small_letters_flag)
    {
        return true;
    }
    return false;
}


function check_email(email)
{
    var shtrudel_flag = false;
    var numbers_flag = false;
    var letters_flag = false;
    var dot_com_flag = email.indexOf(".com") !=-1? true: false; 
    for(i = 0; i < email.length; i++)
    {
        if(email.charAt(i) >= 'a' && email.charAt(i) <= 'z' || email.charAt(i) >= 'A' && email.charAt(i) <= 'Z')
        {
            letters_flag = true;
        }
        
        else if(email.charAt(i) >= '0' && email.charAt(i) <= '9')
        {
            numbers_flag = true;
        }
        else if(email.charAt(i) == '@')
        {
            shtrudel_flag = true;
        }
    }
    if(shtrudel_flag && numbers_flag && letters_flag && dot_com_flag)
    {
        return true;
    }
    return false;
}


function signup()
{
	var name = document.form.name.value
    var username = document.form.username.value
    var password = document.form.password.value
    var email = document.form.email.value
	if(name.length == 0)
	{
		alert("name cant be empty");
		return false;
	}
    if(!check_username(username))
    {
        alert("username can contains only letters and spaces");
        return false;
    }
    if(!check_password(password))
    {
        alert("password should have only:\n-capital letters\n-small letters\n-numbers\n-at least 8 characters");
        return false;
    }
    if(!check_email(email))
    {
        alert("email should contain:\n-letters\n-numbers\n'@' char\n'.com'");
        return false;
    }
    return true;
}


</script>



<body>
<% if(!request.getMethod().equals("POST")){ 
%>

<br><br><br>
<center>
<img src="../images_for_project/design-patterns-logo-2.png" 
     width="500" height="180" alt="webmaster logo" >
</center>
<div id="login-form-wrap">
  <h2>Signup</h2>
  <form id="login-form" action="register.jsp" method="post" name="form" onsubmit="return signup();">
	<p>
    <input type="text" id="name" name="name" placeholder="Name" ><span></span><span></span></i>
    </p>
    <p>
    <input type="text" id="username" name="username" placeholder="Username" ><span></span><span></span></i>
    </p>
    <p>
    <input type="text" id="password" name="password" placeholder="Password" ><span></span><span></span></i>
    </p>
    <p>
    <input type="text" id="email" name="email" placeholder="Enter Email"><span></span><span></span></i>
    </p>
    <div id = "checks">
      <p>Gender:</p>
      <select name="gender">
        <option value="male">Male</option>
        <option value="female">Female</option>
        <option value="other">Other</option>
      </select >
      <p>tell us about yourself:</p>
    <select name="status">
      <option value="university student">university student</option>
      <option value="high school student">high school student</option>
      <option value="army soldier">army soldier</option>
      <option value="company worker">company worker</option>
  </select >
</div>
    <p>
    <!-- <button id="signupButton">signup</button> -->
    <input name="steptwo" value="signup" type="submit" />
    </p>
  </form>
  <div id="create-account-wrap">
    <a href="login.jsp">back to Login</a>
  </div>
  

<%
}// 
   if(request.getMethod().equals("POST"))
  {	    
	String name=request.getParameter("name");
    String username=request.getParameter("username");  
    String password=request.getParameter("password");
    String email=request.getParameter("email");
	String status = request.getParameter("status");
	String gender = request.getParameter("gender");
    String  result [][]= null;	
    try 
	{
	Class.forName("com.mysql.jdbc.Driver").newInstance();	
	 	 	

	   Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/DaniDB","root","d");

	 Statement st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
	  		

     
	  String mySQL = "SELECT * from USERS WHERE email='" + email +"'"; 
	  System.out.println(mySQL);
		  		
	ResultSet oRS = st.executeQuery(mySQL);				

		 			
		int numColumns = oRS.getMetaData().getColumnCount();
		 oRS.last();				
		int numRows = oRS.getRow();
		 	
		 result = new String[numRows][numColumns];	
		 oRS.beforeFirst();
		 int i = 0;
		 while (oRS.next())
		 {
		 	for(int j = 0; j < numColumns; j++ )
		 		result[i][j]=oRS.getString(j+1);				
		 	i++;
		 }		
		 oRS.close();	
		 st.close();	
		 } 
		 catch (Exception e) 
		 {	  			
		    //e.printStackTrace();
		   System.out.println("Register-1- Error in connecting");
		  }

		 if(result.length>0)
		 {
			 out.print("this email was used before");
		 }
		else
			    {
			      try 
				{
				 // create new driver
				 //Class.forName("com.mysql.jdbc.Driver").newInstance(); //already connected to the driver
				 //commit a connection
			 Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/DaniDB","root","d");
			//create a statement
			 Statement st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
			
					
			//insert query
	     String mySQL = "insert into USERS(name,username,password,email,gender,status) " + "values('"+name+"','"+username+"','"+password+"','"+email+"','"+gender+"','"+status+"');";

	System.out.println(mySQL);
		
	//run the query
	int n = st.executeUpdate(mySQL); 
		  				  						
	//oRS.close();	we dont need a result set
	st.close();
	con.close();
	session.setAttribute("IS_LOGGED", "YES");
	response.sendRedirect ("../html_files/logged.html");
	} 
	catch (Exception e) 
	{	  			
	    //e.printStackTrace();
	     System.out.println("Register-2- Error in connecting");
	}    //======================= the connection is closed
			  	} // end else exist
			    	}// end requset post
			    %>	
				
</body>
</html>

