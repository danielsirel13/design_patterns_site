<%@ page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%>
<%@ page import = "java.sql.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
	<meta charset="UTF-8">
  <title>login_DaniSyrel_project</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel="stylesheet" href="../css_files/login_and_signup.css">
<script src="../js_files/jquery-3.6.1.js"></script>
</head>

<script language="javascript">
function check_username(username)
{
    //check if the username contains only letters and spaces
    for(i = 0; i < username.length; i++)
    {
        if((username.charAt(i) < 'A' || username.charAt(i) > 'Z') && (username.charAt(i) < 'a' || username.charAt(i) > 'z') && username.charAt(i) != ' ')
        {
            return false;
        }
    }
    return true;
}

function check_password(password)
{
    var small_letters_flag = false;
    var capital_letters_flag = false;
    var numbers_flag = false;
    //check passwords' length 
    if(password.length < 8)
    {
        return false;
    }
    for(i = 0; i < password.length; i++)
    {
        if(password.charAt(i) >= 'a' && password.charAt(i) <= 'z')
        {
            small_letters_flag = true;
        }
        else if(password.charAt(i) >= 'A' && password.charAt(i) <= 'Z')
        {
            capital_letters_flag = true;
        }
        else if(password.charAt(i) >= '0' && password.charAt(i) <= '9')
        {
            numbers_flag = true;
        }
        else
        {
            return false;
        }
    }
    if(numbers_flag && capital_letters_flag && small_letters_flag)
    {
        return true;
    }
    return false;
}



function login()
{
    debugger;
    var username = document.getElementById('username').value
    var password = document.getElementById("password").value
    if(!check_username(username))
    {
        alert("username can contains only letters and spaces");
        return false;
    }
    if(!check_password(password))
    {
        alert("password should have only:\n-capital letters\n-small letters\n-numbers\n-at least 8 characters");
        return false;
    }
    return true;
}

</script>



<body>

<% if(!request.getMethod().equals("POST")){ 
%>
<br><br><br>
<center>
<img src="../images_for_project/design-patterns-logo-2.png" 
     width="500" height="180" alt="webmaster logo" >
</center>
<div id="login-form-wrap">
  <h2>Login</h2>
  <form id="login-form" action="login.jsp" method="post" name="form" onsubmit="return login();">
    <p>
    <input type="text" id="username" name="username" placeholder="Username" ><span></span><span></span></i>
    </p>
    <p>
    <input type="text" id="password" name="password" placeholder="Password" ><span></span><span></span></i>
    </p>
    <p>
    <input name="steptwo" value="login" type="submit" />
    <!-- <button onclick="login();" id="LoginBtn" type="submit">login</button> -->

    </p>
  </form>
  <div id="create-account-wrap">
    <p>Not a member? <a href="register.jsp">Create Account</a><p>
  </div>
  
  

<%
}// 
   if(request.getMethod().equals("POST"))
  {	     
    String username=request.getParameter("username");  
    String password=request.getParameter("password");
    String  result [][]= null;	
    try 
	{
	Class.forName("com.mysql.jdbc.Driver").newInstance();	
	 	 	
 // start the driver
	   Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/DaniDB","root","d");
	 	
	 //create statement
	 Statement st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_UPDATABLE);
	  		
	// Statement st = con.createStatement(); // ׳›׳�׳©׳¨ ׳�׳™׳� ׳₪׳¨׳�׳˜׳¨׳™׳�, ׳�׳– ׳�׳™ ׳�׳₪׳©׳¨ ׳�׳ ׳•׳¢ ׳�׳�׳¢׳�׳” - ׳�׳�׳˜׳” ׳‘׳×׳•׳� ׳”׳¨׳§׳•׳¨׳“׳¡׳˜
				
	
     
	String mySQL = "SELECT * FROM USERS WHERE username='" + username + "' AND password ='" + password + "'" ;
	//create the result set, it is set that contains the db in it
	ResultSet resultSet = st.executeQuery(mySQL);				
	//end of the connection with the db
	session.setAttribute("MyADMIN", "NotOK");			
	resultSet.last();		
	int numRows = resultSet.getRow();
	if (numRows > 0)	
	   {	   
	    session.setAttribute("IS_LOGGED", "YES");
		response.sendRedirect ("../html_files/logged.html");
		resultSet.close();
		st.close();
	   }
	 else
	 {
		out.print("error - username or password is uncorrect");
	 }
	resultSet.close();
	st.close();
	}
	  catch (Exception e) 
	  {
		System.out.println("Error in connection"+e);
	} //the connection is closed	  		
			
	}	
    %>		
				
</body>
</html>

