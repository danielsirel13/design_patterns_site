<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title> Administrator Page </title> 
</head>
<body dir="rtl" style bgcolor = #737373>

<%
if ((session.getAttribute("MyADMIN")== null)|| !session.getAttribute("MyADMIN").equals("OK")) // checking if the user is admin and if he is allowed to be entered to the manage page
			response.sendRedirect ("../html_files/No_Admin_manage.html");
      %>
      
<center>
 <h1>Admin permissions page </h1> 
<div>
 <h2>Admin Menu</h2>
 <form id="managerForm" action="Admin_manage.jsp" method="post">
 <table>
   <tr>
   <th colspan="2">show all the fields of all of the users</th>
   <td>
    <input type="submit" id="allUsers" name="sendUser" value="Show All" style="height: 25px; width: 200px">
   </td>
   </tr>
   <tr>
   <th colspan="2">Delete a user by a email</th>
   <td>
    <input type="submit" id="deletUser" name="sendUser" value="Delete By Email" style="height: 25px; width: 200px">
   </td>
   </tr>
   <tr>
    <th colspan="2">Delete a user by a username </th>
    <td>
     <input type="submit" id="deletUser" name="sendUser" value="Delete By Username" style="height: 25px; width: 200px">
    </td>
    </tr>
   <tr>
   <th colspan="2"> update fields by a username </th>
   <td>
   <input type="submit" id="updateUser1" name="sendUser" VALUE="Update By Username" style="height: 25px; width: 200px">
   </td>
    <tr>
   <th colspan="2"> insert new admin </th>
   <td>
   <input type="submit" id="insertAdmin" name="sendUser" VALUE="insert new admin" style="height: 25px; width: 200px">
   </td>
   </tr>
   
 </table>
 </form>
</div>
</center>
<%

String s=request.getParameter("sendUser");

try {
	if(s.equals("Show All"))
	{
		response.sendRedirect("select.jsp");
	}
	else if(s.equals("Delete By Email"))
	{
		response.sendRedirect("delete_email.jsp");
	}
  else if(s.equals("Delete By Username"))
	{
		response.sendRedirect("delete_username.jsp");
	}
	else if(s.equals("Update By Username"))
	{
		response.sendRedirect("update_username.jsp");
	}
	else if(s.equals("insert new admin"))
	{
		response.sendRedirect("new_admin.jsp");
	}
	}
catch(Exception e)
{
	System.out.println("Error" + e);
}
%>
</body>
</html>