function signup(){
    debugger;
    var username = document.getElementById('username').value
    var password = document.getElementById("password").value
    var email = document.getElementById("email").value
    if(!check_username(username))
    {
        alert("username can contains only letters and spaces");
        return false;
    }
    if(!check_password(password))
    {
        alert("password should have only:\n-capital letters\n-small letters\n-numbers\n-at least 8 characters");
        return false;
    }
    if(!check_email(email))
    {
        alert("email should contain:\n-letters\n-numbers\n'@' char\n'.com'");
        return false;
    }
    return true;
}


function login()
{
    debugger;
    var username = document.getElementById('username').value
    var password = document.getElementById("password").value
    if(!check_username(username))
    {
        alert("username can contains only letters and spaces");
        return false;
    }
    if(!check_password(password))
    {
        alert("password should have only:\n-capital letters\n-small letters\n-numbers\n-at least 8 characters");
        return false;
    }
    return true;
}

function check_username(username)
{
    //check if the username contains only letters and spaces
    for(i = 0; i < username.length; i++)
    {
        if((username[i] < 'A' || username[i] > 'Z') && (username[i] < 'a' || username[i] > 'z') && username[i] != ' ')
        {
            return false;
        }
    }
    return true;
}

function check_password(password)
{
    var small_letters_flag = false;
    var capital_letters_flag = false;
    var numbers_flag = false;
    //check passwords' length 
    if(password.length < 8)
    {
        return false;
    }
    for(i = 0; i < password.length; i++)
    {
        if(password[i] >= 'a' && password[i] <= 'z')
        {
            small_letters_flag = true;
        }
        else if(password[i] >= 'A' && password[i] <= 'Z')
        {
            capital_letters_flag = true;
        }
        else if(password[i] >= '0' && password[i] <= '9')
        {
            numbers_flag = true;
        }
        else
        {
            return false;
        }
    }
    if(numbers_flag && capital_letters_flag && small_letters_flag)
    {
        return true;
    }
    return false;
}


function check_email(email)
{
    var shtrudel_flag = false;
    var numbers_flag = false;
    var letters_flag = false;
    var dot_com_flag = email.indexOf(".com") !=-1? true: false; 
    for(i = 0; i < email.length; i++)
    {
        if(email[i] >= 'a' && email[i] <= 'z' || email[i] >= 'A' && email[i] <= 'Z')
        {
            letters_flag = true;
        }
        
        else if(email[i] >= '0' && email[i] <= '9')
        {
            numbers_flag = true;
        }
        else if(email[i] == '@')
        {
            shtrudel_flag = true;
        }
    }
    if(shtrudel_flag && numbers_flag && letters_flag && dot_com_flag)
    {
        return true;
    }
    return false;
}